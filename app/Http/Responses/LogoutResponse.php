<?php

namespace App\Httpd\Responses;

use Laravel\Fortify\Contracts\LogoutResponse as LogoutResponseContract;

class LogoutResponse implements LogoutResponseContract
{

    /**
     * toResponse
     * 
     * @param mixed $reques
     * @return void
     */
    public function toResponse($reques)
    {
        return redirect('/login');
    }
}