<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Navigasi</title>
    <link rel="stylesheet" href="{{ asset('admin/css/navbar.css') }}">
</head>
<body>
    <div class = "container">
        <nav class="navbar">
            <a href="" class="link link--aktif">Home</a>
            <a href="" class="link">About</a>
            <a href="" class="link">Project</a>
            <a href="" class="link">Services</a>
            <a href="" class="link">Contact</a>
        </nav>
    </div>
    <section class="bang--renol">
        <img src="{{ asset('images/sunset.jpg') }}" alt="" class="cepmek">
    </section>
</body>
</html>